#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>


#include <iostream> 

#include "mems.h"


Cmems::Cmems( const char *COM_name, unsigned long speed, const char *FILE_name, 
	      unsigned long IPaddr, unsigned short IPport) :
_ch_ok(false), _f_ok(false), _an_ok(false), _tmr_ok(false), _cl_ok(false) 
{
	std::cout << "create mems object\n";

	_mutex = PTHREAD_MUTEX_INITIALIZER;

	_ch = new Cchannel(COM_name, speed, _bufSize/2);

	if (_ch == NULL)
	{
		// handle error
		std::cerr << "error: couldn't creat channel" << std::endl;
	}
	else 
	{
		_ch_ok = true;
	}




	if (_ch_ok) // если канал создан успешно
	{


		_buf = new char [_bufSize]; // выделение памяти под буфер 
		if(_buf == NULL) {
			// handle error
			std::cerr << "error: couldn't creat buffer" << std::endl;
			_ch_ok = false;
		}	

		_fout.open(FILE_name);
		if (!_fout.is_open())
		{
			std::cerr << "error: file is not opened\n"; 		
		}
		else 
		{
			_f_ok = true;
		}
	 
		_an = new Analyser(_analyserBufSize);
		if (_an == NULL) {
			std::cerr << "error: couldn't creat analyser\n"; 	
		}
		else 
		{
			_an_ok = true;
		}


#if 1
		tHandlerStruct th;
		th.func = Cmems::timerFunc;
		th.param = (void* )this;

		_tmr = new Timer(_timerPeriod, th);
		if (_tmr == NULL) {
			std::cerr << "error: couldn't creat timer\n"; 					
		}
		else 
		{
			_tmr_ok = true;
		}
#endif
			
		_cl = new Client(IPaddr, IPport);
		if (_cl == NULL) {
			std::cerr << "error: couldn't creat UDP client\n"; 					
		}
		else 
		{
			_clBuf = new char[sizeof(mems_t) + 2];
			if (_clBuf != NULL) _cl_ok = true;
		}


		
	}
	

	
}

Cmems::~Cmems() 
{
	std::cout << "destroy mems object\n";

	if (_ch_ok) delete _ch; // освобождение памяти, выделенной под канал

	if (_buf != NULL) delete[] _buf; // освобождение памяти, выделенной под буфер

	if (_f_ok) _fout.close(); // закрытие файла

	if (_an_ok) delete _an; // освобождение памяти, выделенной под анализатор

	if (_tmr_ok) delete _tmr; // освобождение памяти, выделенной под таймер

	if (_cl_ok) // закрытие клиента UDP и освобождение памяти
	{
		_cl->closeConnect();
		delete _cl;
		if (_clBuf != NULL) delete _clBuf;
	}
}

int Cmems::update()
{
//    std::cout << "mems update\n";
    int r;

//		usleep(1000);

    r=_ch->read();	
//    std::cout << "r=" << r << "\n";
    if (r>0)
    {
			_ch->getData(_buf, r);

			

#if 0
        for (int i=0; i<r; ++i)
        {
      //      std::cout << _buf[i];
	    		_fout << _buf[i];
        }
#endif

			_lock();
			_an->check(_buf,r);
			_unlock();

  		_ch->delData(r);

//	mems_t m;
//	if (filter(m) > 0) sender(m);
    }

    //usleep(1000);

    return 0;
}


void Cmems::timerFunc(void * param)
{
	//std::cout << "mems timer func\n";
	if (param != NULL)
	{
		// получаем указатель на объект mems 
		Cmems * ptr = reinterpret_cast<Cmems *> (param);
		
		mems_t m;

		ptr->_lock();
		int r = ptr->filter(m);
		ptr->_unlock();

		if (r) ptr->sender(m);

	}
}

// функция фильтрации данных
int Cmems::filter(mems_t & m)
{
	//std::cout << "mems filter func\n";

	int res = -1;

	if (_an_ok)
	{
		res = 0;

		const mems_t * ptr = _an->getMemsDataPtr();		
		size_t sz = _an->getMemsDataCnt();

		std::cout << "cnt="<< sz << "\n";

		if (sz) 
		{
			
					
#if 1
			// усреднение для значений датчиков
			mems_long_t ml;
			memset(&ml, 0, sizeof(ml));
			for (size_t i=0; i<sz; ++i)
			{
					ml.x_gyro += ptr->x_gyro;
					ml.y_gyro += ptr->y_gyro;
					ml.z_gyro += ptr->z_gyro;
					ml.x_accl += ptr->x_accl;
					ml.x_accl += ptr->x_accl;
					ml.y_accl += ptr->y_accl;
					ml.z_accl += ptr->z_accl;
					ml.temper += ptr->temper;
					++ptr;
			}
	
			// флаги и счетчик берем последние
			--ptr;
			m.flags1 = ptr->flags1;
			m.flags2 = ptr->flags2;
			m.cntr = ptr->cntr;
			
			// а значения датчиков средние
			m.x_gyro = ml.x_gyro / sz;
			m.y_gyro = ml.y_gyro / sz;
			m.z_gyro = ml.z_gyro / sz;
			m.x_accl = ml.x_accl / sz;
			m.y_accl = ml.y_accl / sz;
			m.z_accl = ml.z_accl / sz;
			m.temper = ml.temper / sz;			
#endif		

#if 0
			// пока не фильтруем, а просто берем первую запись			
			m = *ptr; 
#endif 

#if 1
			//std::cout.unsetf(std::ios::dec);
			//std::cout.setf(std::ios::hex);	
			std::cout <<
			"f1=" << m.flags1  <<
			" f2=" << m.flags2  <<
			" xg=" << (int16_t)m.x_gyro / 2  <<				
			" yg=" << (int16_t)m.y_gyro / 2  <<
			" zg=" << (int16_t)m.z_gyro / 2  <<
			" xa=" << (int16_t)m.x_accl / 4  <<
			" ya=" << (int16_t)m.y_accl / 4  <<
			" za=" << (int16_t)m.z_accl / 4  <<
			" t=" << m.temper/20 + 25  <<
			" cnt=" << m.cntr << "\n";
			//std::cout.unsetf(std::ios::hex);
#endif
			_an->clear();
			res = 1;
		}

	}

	return res;

}

// функция отправки данных MEMS по UDP
void Cmems::sender(mems_t & m)	
{	

	//if (_cl_ok) _cl->sendMsg(&m, sizeof(m));

	if (_cl_ok) // посылаем данные + crc16 по UDP
	{

		memcpy(_clBuf, &m, sizeof(mems_t));

		_clBuf[sizeof(mems_t)] = 0;
		_clBuf[sizeof(mems_t)+1] = 0;	
		uint16_t crc = _an->calcCRC16(_clBuf, sizeof(mems_t) + 2);		
		_clBuf[sizeof(mems_t)] = crc & 0xff;
		_clBuf[sizeof(mems_t)+1] = (crc >> 8) & 0xff;

		_cl->sendMsg(_clBuf, sizeof(mems_t) + 2);
	}
	
}





