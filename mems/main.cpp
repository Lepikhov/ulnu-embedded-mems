#include <iostream>
#include <fstream>
#include <string>
#include <sys/socket.h>
#include <netinet/in.h>
#include <cstring>
#include <cstdlib>
#include <sys/resource.h>
#include <sys/types.h>
#include <unistd.h>


#include "mems.h"
 
#define COM_NAME_MAX_SIZE 50 // максимальная длина имени com-порта
#define FILE_NAME_MAX_SIZE 255 // максимальная длина имени файла

void IPaddrToStr( unsigned int ip, char * str)
{
	sprintf(str, "%u.%u.%u.%u", (ip>>24)&255, (ip>>16)&255, (ip>>8)&255, (ip)&255);
}
 
int main(int argc, char* argv[])
{


	char COM_name[COM_NAME_MAX_SIZE] = "/dev/ttyUSB0";
	unsigned long speed = 921600;
	char FILE_name[FILE_NAME_MAX_SIZE] = "mems.txt";
	unsigned long IPaddr = INADDR_LOOPBACK;
	unsigned short IPport = 3425; 

	char addrBuf[16];

	if (argc > 1)
	{

		if ((argc == 2) && ( !strcmp(argv[1], "-h") || !strcmp(argv[1], "--h")) )
		{
			std::cout << "Usage:\n" 
			<< "memtest COM_name COM_speed FILE_name IPaddr IPport\n"
			<< "Defaults:\n";

			IPaddrToStr(IPaddr, addrBuf);

			std::cout  
			<< "COM_name: " << COM_name << "\n"
			<< "COM_speed: " << speed << "\n"
			<< "FILE_name: " << FILE_name << "\n"
			<< "IPaddr: " << addrBuf << "\n"
			<< "IPport: " << IPport << "\n";

			return 0;
		} 

		if (strlen(argv[1]) > COM_NAME_MAX_SIZE)
		{
			std::cout << "COM_name is to long. The default value will be used (" << COM_name << ")\n";
		}	
		else
		{
			strncpy(COM_name, argv[1], strlen(argv[1]));
			COM_name[strlen(argv[1])] = '\0';
		}
	
		if (argc > 2) 
		{
			unsigned long s = strtoul(argv[2], NULL, 0);
			if (!s)
			{
				std::cout << "COM_speed is wrong. The default value will be used (" << speed << ")\n";
			}	
			else
			{
				speed = s;
			}

		}
		
		if (argc > 3)
		{
			if (strlen(argv[3]) > FILE_NAME_MAX_SIZE)
			{
				std::cout << "FILE_name is to long. The default value will be used (" << FILE_name << ")\n";
			}	
			else
			{
				strncpy(FILE_name, argv[3], strlen(argv[3]));
				FILE_name[strlen(argv[3])] = '\0';
			}

		}

		if (argc > 4)
		{

			unsigned int b0, b1, b2, b3;
 			if (sscanf(argv[4], "%i.%i.%i.%i", &b3, &b2, &b1, &b0) != 4)
			{
			
				IPaddrToStr(IPaddr, addrBuf);
				std::cout << "IPaddr is wrong. The default value will be used (" << addrBuf << ")\n";
			}
			else
			{
				if ( (b0 > 255) || (b1 > 255) || (b2 > 255) || (b3 > 255))
				{
					IPaddrToStr(IPaddr, addrBuf);
					std::cout << "IPaddr is wrong. The default value will be used (" << addrBuf << ")\n";
				}
				else
				{
					IPaddr = ((b3&255)<<24) | ((b2&255)<<16) | ((b1&255)<<8) | (b0&255);
					
				}	
			}			

		}

		if (argc > 5) 
		{
			unsigned long p = strtoul(argv[5], NULL, 0);
			if (!p)
			{
				std::cout << "IPport is wrong. The default value will be used (" << IPport << ")\n";
			}	
			else 
			{
				IPport = p;
			}

		}
	}

	IPaddrToStr(IPaddr, addrBuf);

	std::cout  
	<< "COM_name: " << COM_name << "\n"
	<< "COM_speed: " << speed << "\n"
	<< "FILE_name: " << FILE_name << "\n"
	<< "IPaddr: " << addrBuf << "\n"
	<< "IPport: " << IPport << "\n";

//	Cmems mems("/dev/ttymxc1", 115200, "mems.txt");
//	Cmems mems("/dev/ttyUSB0", 921600, "mems.txt", INADDR_LOOPBACK, 3425);
//	Cmems mems("/dev/ttymxc1", 921600, "mems.txt", INADDR_LOOPBACK, 3425);
//	Cmems mems("/dev/ttyS1MAX2", 921600, "mems.txt", INADDR_LOOPBACK, 3425);
	int priority = getpriority(PRIO_PROCESS, getpid());
	priority -= 20;
//	std::cout  << "setpriority:" << 
	setpriority(PRIO_PROCESS, getpid(),priority); 
//	<< std::endl;
	sleep(1);
	priority = getpriority(PRIO_PROCESS, getpid());
	std::cout  << "current priority:" << priority << std::endl;

	Cmems mems(COM_name, speed, FILE_name, IPaddr, IPport);

	while (1)
	{
		mems.update();
	}
 


	

}
