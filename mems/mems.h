#ifndef MEMS_H_
#define MEMS_H_


#include <fstream>
#include <pthread.h>
#include "../channel/channel.h"
#include "../analyser/analyser.h"
#include "../timer/timer.h"
#include "../client/client.h"

/// класс для работы с датчиком MEMS
/// получает сырые данные от датчика,
/// анализирует сырые данные,
/// по событию от таймера (100 млсек)
/// фильтрует данные и посылает на udp-сервер одну запись текущих показаний датчика

class Cmems
{
  private:
	static const unsigned int _bufSize = 1000; ///< размер буфера данных в канале связи с датчиком MEMS
	static const unsigned int _analyserBufSize = 300; ///< размер буфера записей в анализаторе 
	static const unsigned int _timerPeriod = 100; ///< период срабатывания таймера (млсек)  


	Cchannel *_ch; 		///< указатель на канал связи с модулем MEMS
	bool _ch_ok; 		///< статус канала связи с модулем MEMS

	char * _buf;  		///< указатель на буфер 
	std::ofstream _fout;	///< выходной файл	
	bool _f_ok;		///< статус открытия файла

	Analyser *_an;  	///< указатель на объект анализатора сообщений от модуля MEMS
	bool _an_ok; 		///< статус объекта анализатора

	Timer *_tmr; 	///< указатель на таймер
	bool _tmr_ok; 	///< статус объекта таймера

	Client *_cl;	///< указатель на клиента UDP
	bool _cl_ok; 	///< статус объекта клиента UDP
	char *_clBuf; 	///< указатель на буфер для сообщений, передаваемых по UDP

	pthread_mutex_t _mutex; ///< мутекс для работы с таймером

	void _lock() {pthread_mutex_lock(&_mutex);} ///< блокировка мутекса	
	void _unlock() {pthread_mutex_unlock(&_mutex);} ///< разблокировка мутекса	


  public:
	Cmems( const char *COM_name, unsigned long speed, const char *FILE_name, 
		unsigned long IPaddr, unsigned short IPport); ///< конструктор класса
	~Cmems(); ///< деструктор класса
	int update();///< обновление данных от платы MEMS
	int filter(mems_t & m); ///< функция фильтрации данных, результат в m
	void sender(mems_t & m); ///< функция отправки данных MEMS по UDP
	static void timerFunc(void * param); ///< функция, вызываемая из таймера
};



#endif /*MEMS_H_*/
