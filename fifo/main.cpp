#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <iostream> 

using std::cout;

#include "fifo.h"


int main(int parm_count, char *parms[])
{
	circular_buffer cb;
	int tmp;	
	int c;
	unsigned char buf[10];
	
	cb_init(&cb, 3, 1);

	tmp = 30;
	cb_push_back(&cb, &tmp);

	tmp = 31;
	cb_push_back(&cb, &tmp);

	tmp = 32;
	cb_push_back(&cb, &tmp);

	tmp = 33;
	cb_push_back(&cb, &tmp);

	c = cb_get_buffer(&cb, buf, 4);
	cout << "всего " << c << " элем.:" << std::endl;
	for (int i=0; i<c; ++i) {
		tmp = buf[i];
       		cout << i+1 <<"-й элемент " << tmp <<" ";
	}
	cout << std::endl;

	cb_pop_front(&cb, &tmp);
	cout << "1-й элемент " << tmp << std::endl;

	cb_pop_front(&cb, &tmp);
	cout << "2-й элемент " << tmp << std::endl;

//	cb_pop_front(&cb, &tmp);
//	cout << "3-й элемент " << tmp << std::endl;

//	cb_pop_front(&cb, &tmp);

	tmp = 34;
	cb_push_back(&cb, &tmp);

	tmp = 35;
	cb_push_back(&cb, &tmp);

	c = cb_get_buffer(&cb, buf, 4);
	cout << "всего " << c << " элем.:" << std::endl;
	for (int i=0; i<c; ++i) {
		tmp = buf[i];
       		cout << i+1 <<"-й элемент " << tmp <<" ";
	}
	cout << std::endl;

	cb_free(&cb);


	return 0;
}

