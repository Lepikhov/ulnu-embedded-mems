#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

#include <iostream> 


#include "fifo.h"


int cb_init(circular_buffer *cb, size_t capacity, size_t sz)
{
	if (sz == 0) {
		// handle error
		std::cerr << "error: size of each item = 0" << std::endl;
		return -1;
	}
	if (capacity == 0) {
		// handle error
		std::cerr << "error: capacity = 0" << std::endl;
		return -1;
	}
	//cb->buffer = malloc(capacity * sz);
	cb->buffer = new char [capacity * sz];
	if(cb->buffer == NULL) {
		// handle error
		std::cerr << "error: couldn't creat buffer" << std::endl;
		return -1;
	}	
	cb->buffer_end = (char *)cb->buffer + capacity * sz;
	cb->capacity = capacity;
	cb->count = 0;
	cb->sz = sz;
	cb->head = cb->buffer;
	cb->tail = cb->buffer;

	return 0; 	
}

void cb_free(circular_buffer *cb)
{
	//free(cb->buffer);
	if (cb->buffer != NULL) delete[] (char *)cb->buffer;
	// clear out other fields too, just to be safe
}

int cb_push_back(circular_buffer *cb, const void *item)
{
	if(cb->count == cb->capacity) {
		// handle error
		std::cerr << "error: buffer is full" << std::endl;
		return -1;
	}
	memcpy(cb->head, item, cb->sz);
	cb->head = (char*)cb->head + cb->sz;
	if(cb->head == cb->buffer_end)
		cb->head = cb->buffer;
	cb->count++;	
	return 0; 
}

int cb_pop_front(circular_buffer *cb, void *item)
{
	if(cb->count == 0) {
		// handle error
		std::cerr << "error: buffer is empty" << std::endl;
		return -1; 
	}
	memcpy(item, cb->tail, cb->sz);
	cb->tail = (char*)cb->tail + cb->sz;
	if(cb->tail == cb->buffer_end)
		cb->tail = cb->buffer;
	cb->count--;

	return 0; 
}

int cb_delete(circular_buffer *cb, size_t qty)
{
	size_t tmp;

	if (cb->sz == 0) {
		// handle error
		std::cerr << "error: size of each item = 0" << std::endl;
		return -1;
	}

	if ((qty == 0) || (qty >= cb->count)) {
		// delete all
		cb->tail = cb->head;
		cb->count = 0;
		return 0;				
	}

	if (cb->tail < cb->head) {
		cb->tail = (char*)cb->tail + qty*cb->sz;	
	}
	else {
		tmp = ((char*)cb->buffer_end - (char*)cb->tail)/cb->sz;
		if (qty <= tmp) {
			cb->tail = (char*)cb->tail + qty*cb->sz;
		}
		else {
			cb->tail = (char*)cb->buffer + (qty-tmp)*cb->sz;
		}
	}

	cb->count -= qty;
	return 0;
	
	
}

int cb_get_buffer(circular_buffer *cb, void *dst, size_t qty) 
{
	size_t tmp;

	if (cb->sz == 0) {
		// handle error
		std::cerr << "error: size of each item = 0" << std::endl;
		return -1;
	}

	if (qty == 0) return 0;
	if (qty >= cb->count) qty = cb->count;

	if (cb->tail < cb->head) {
		memcpy(dst, cb->tail, qty*cb->sz);
	}
	else {
		tmp = ((char*)cb->buffer_end - (char*)cb->tail)/cb->sz;
		if (qty <= tmp) {
			memcpy(dst, cb->tail, qty*cb->sz);
		}
		else {
			memcpy(dst, cb->tail, tmp*cb->sz);
			dst = (char*)dst + tmp*cb->sz;
			memcpy(dst,(char*)cb->buffer,(qty-tmp)*cb->sz);
		}
		
	}
	return (int)qty;
}


