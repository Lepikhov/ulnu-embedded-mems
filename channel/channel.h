#ifndef CHANNEL_H_
#define CHANNEL_H_

#include "../fifo/fifo.h"
#include "../serialfunc/serialfunc.h"

///класс канала связи с оборудованием
class Cchannel
{
  private:
	//static const unsigned int _sBufSize = 1000; ///< размер буфера последовательного порта
	//static const unsigned int _cBufSize = 1000; ///< размер кольцевого буфера
	static const unsigned int _maxRqty = 100; ///< максимальное число принимаемых байт 

	circular_buffer _cb; 	///< кольцевой буфер
	bool _cb_ok; 		///< статус кольцевого буфера

	int _sch;	///< канал последовательного порта
	bool _sch_ok; 	///< статус канал последовательного порта
	unsigned char * _srbuf;  	///< указатель на приемный буфер последовательного порта   
	unsigned char * _swbuf;  	///< указатель на буфер передачи последовательного порта  

	char *_COM_name;    			///< буфер под имя порта
	unsigned long _speed;			///< заданная скорость связи

  public:
	Cchannel( const char *COM_name, unsigned long speed, const unsigned int sBufSize); ///< конструктор класса
	~Cchannel();
	int read(); ///< чтение канала
	int getData(void * dst, unsigned int qty);///< получение данных из канала
	int delData( unsigned int qty); ///< удаление данных из канала
};



#endif /*CHANNEL_H_*/
