#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

#include <iostream> 

#include "channel.h"


Cchannel::Cchannel( const char *COM_name, unsigned long speed, const unsigned int sBufSize) : _cb_ok(false), _sch_ok(false) 
{
	std::cout << "create channel\n";

	if (!cb_init(&_cb, sBufSize, 1)) _cb_ok = true; // создание кольцевого буфера
	
	if (_cb_ok) // если кольцевой буфер создан успешно
	{
		_sch_ok = openPort(&_sch, COM_name, speed); // открытие канала последовательного порта

		// сохраняем скорость и имя порта
		_speed = speed;
		_COM_name = new char [strlen(COM_name)+1];

		if (_COM_name == NULL)
		{
			_sch_ok = false;
			return;
		}
		memcpy(_COM_name, COM_name, strlen(COM_name)+1);

		_srbuf = new unsigned char [sBufSize]; // выделение памяти под буфер ввода
		if(_srbuf == NULL) {
			// handle error
			std::cerr << "error: couldn't creat srbuffer" << std::endl;
		}	

		_swbuf = new unsigned char [sBufSize]; // выделение памяти под буфер вывода
		if(_swbuf == NULL) {
			// handle error
			std::cerr << "error: couldn't creat swbuffer" << std::endl;
		}
		
	}

	
}

Cchannel::~Cchannel() 
{
	std::cout << "destroy channel\n";

	if (_cb_ok) cb_free(&_cb); // освобождение памяти, выделенной под кольцевой буфер

	if (_sch_ok) // закрытие последовательного порта и освобождения памяти, выделенной под буферы
	{
		closeCom(&_sch);

		if (_srbuf != NULL)
		{
			delete[] _srbuf;
		}

		if (_swbuf != NULL)
		{
			delete[] _swbuf;
		}
	}
}

int Cchannel::read() 
{
	int r, t;
	
//	std::cout << "channel read\n";
	
	if (_cb_ok && _sch_ok) 
	{
		r = readData(&_sch, _srbuf, _maxRqty);
		if(r <= 0) 
		{
			//if (r==0) 
			{
				std::cerr << "timeout\n";
				clrInputBuffer(&_sch);
			//else {
				closeCom(&_sch);
				//usleep(100000);	
				_sch_ok = openPort(&_sch, _COM_name, _speed); // открытие канала последовательного порта
			}
			return r; //-2;
		}
		else
		{
//			std::cout << "received " << r << " byte(s)\n";
#if 1
			for (int i = 0; i < r; ++i)
			{
				t = cb_push_back(&_cb, &_srbuf[i]);
				if (t < 0) return -3;
			}	
#endif
		}

	}
	else
	{
		return -1;
	}

	return r;
}

int Cchannel::getData(void * dst, unsigned int qty) 
{
//	std::cout << "channel get data\n";
	if (_cb_ok)
	{
	    return cb_get_buffer(&_cb, dst, qty);
	}
	else
	{
	    return -1;
	}
}

int Cchannel::delData(unsigned int qty) 
{
//	std::cout << "channel del data\n";
	if (_cb_ok)
	{
		return cb_delete(&_cb, qty);
	}
	else
	{
	    return -1;
	}

}




