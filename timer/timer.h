#ifndef _TIMER_H
#define _TIMER_H


#include <signal.h>
#include <sys/time.h>


///тип указателя на функцию, вызываемую по событию от таймера
typedef void (*tHandlerFuncPtr)(void * param);


///структура описания функции, вызываемой по событию от таймера
typedef struct {
	void (*func)( void *param); ///< указатель на функцию
	void * param; ///< указатель на параметры, передаваемые в функцию
} tHandlerStruct;


/// Класс для работы с интервальным таймером

class Timer {
private:
	/// период срабатывания таймера (в млсек)
	unsigned int _period;


	// структуры для настройки таймера
	/// itimerspec структура для таймера
	struct itimerspec _timerSpecs;

	/// указатель на описание пользовательской функции, вызываемой по событию от таймера
	//void (*_handler)( void *param);  
	tHandlerStruct _handler;

	/// Определяет action для сигнала таймера 
	struct sigaction _signalAction;

	/// Stored timer ID for alarm
	timer_t _timerID;
 
	/// Signal blocking set
	sigset_t _sigBlockSet;
 
	/// структура события для сигнала таймера, через нее передается указатель на объект таймера
	struct sigevent _signalEvent;
	
	/// обработчик сигнала от таймера
	static void _handle(int sigNumb, siginfo_t *si, void *uc);



public:
	/// конструктор класса: period - период срабатывания таймера (в млсек), 
	/// handler - указатель на структуру функции-handler'a (если == NULL, то нет функции)
	//Timer(unsigned int period, void (*handler)( void *param));	
	Timer(unsigned int period, tHandlerStruct  handler);

};


#endif //_TIMER_H 
