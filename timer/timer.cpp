#include <unistd.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <assert.h>
#include <string.h>


#include "timer.h"



// конструктор класса: period - период срабатывания таймера (в млсек), 
// handler - указатель на функцию (если == NULL, то нет функции)
//Timer::Timer(unsigned int period, void (*handler)( void *param)) : _period(period), _handler(handler) 
Timer::Timer(unsigned int period, tHandlerStruct  handler) : _period(period), _handler(handler) 
{



	// старт таймера через _period млсек
	this->_timerSpecs.it_value.tv_sec = _period / 1000;
	this->_timerSpecs.it_value.tv_nsec = (_period % 1000)*1000000;
	// период возникновения событий = _period млсек
	this->_timerSpecs.it_interval.tv_sec = _period / 1000;
	this->_timerSpecs.it_interval.tv_nsec = (_period % 1000)*1000000;

	// очищаем sa_mask
	sigemptyset(&this->_signalAction.sa_mask);
	// устанавливаем SA_SIGINFO флаг, чтобы использовать функцию-handler 
	this->_signalAction.sa_flags = SA_SIGINFO;
 
	
	// функция _handle будет вызываться при возникновении сигнала 
	this->_signalAction.sa_sigaction = Timer::_handle;

	// определяем sigEvent
	// Эта информация будет передоваться в функцию _handle
	memset(&this->_signalEvent, 0, sizeof(this->_signalEvent));
	// With the SIGEV_SIGNAL flag we say that there is sigev_value
	this->_signalEvent.sigev_notify = SIGEV_SIGNAL;
	// передаем указатель на объект таймера
	this->_signalEvent.sigev_value.sival_ptr = (void*) this;
	// определяем  сигнал как SIGUSR1// Alarm Signal
	this->_signalEvent.sigev_signo = SIGUSR1; //SIGALRM;

	// создаем таймер
	if (timer_create(CLOCK_REALTIME, &this->_signalEvent, &this->_timerID) != 0) 
	{ 
		std::cerr << "Could not creat the timer\n";
		exit(1);
	}
 

	// инициализируем действие сигнала
	if (sigaction(/*SIGALRM*/SIGUSR1, &this->_signalAction, NULL)) {
		std::cerr << "Could not install new signal handler\n";
	}



	// запускаем таймер
	if (timer_settime(this->_timerID, 0, &this->_timerSpecs, NULL) == -1) 
	{
		std::cerr << "Could not start timer:\n";
	}
}	


// обработчик сигнала от таймера
void Timer::_handle(int sigNumb, siginfo_t *si, void *uc)
{
	assert(sigNumb == SIGUSR1); //SIGALRM);

	//std::cout << "_handle is executed\n";

	// получаем указатель на объект таймера из структуры siginfo 
	Timer * ptr = reinterpret_cast<Timer *> (si->si_value.sival_ptr);
	// вызываем пользовательскую функцию
	/*if (ptr->_handler != 0)*/ ptr->_handler.func(ptr->_handler.param);



}
