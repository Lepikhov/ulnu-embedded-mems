#ifndef CLIENT_H
#define CLIENT_H

/// класс udp-клиента
class Client
{
private:
	int _sock; ///< дескриптор сокета
	bool _isOpened; ///< флаг открытия сокета (==true - сокет открыт)
public:
	Client(unsigned long addr, unsigned short int port); ///< конструктор класса
	~Client() {closeConnect();} ///< деструктор класса	
	ssize_t sendMsg(const void* msg, ssize_t size); ///< отправка сообщения 		
	ssize_t receiveMsg(void* msg, ssize_t size); ///< прием сообщения
	int closeConnect(); ///< закрытие соединения	
};


#endif // CLIENT_H
