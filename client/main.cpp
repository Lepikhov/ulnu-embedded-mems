#include <iostream>
#include <fstream>

#include <unistd.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include "client.h"




char msg[] = "Hello there!\n";


int main()
{

	Client cl = Client(INADDR_LOOPBACK, 3425); 
    
	while (1) 
	{				
		cl.sendMsg(msg, sizeof(msg));		
		sleep(5);
	} 	

	cl.closeConnect();	
		


	return 0;
}

