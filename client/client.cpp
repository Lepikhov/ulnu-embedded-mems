#include <unistd.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include "client.h"

// конструктор класса
Client::Client(unsigned long addr, unsigned short int port)
{
	struct sockaddr_in address;

	_sock = socket(AF_INET, SOCK_DGRAM, 0);
	if(_sock < 0)
	{
		_isOpened = false;
		return;
	}

	
	address.sin_family = AF_INET;
	address.sin_port = htons(port);
	address.sin_addr.s_addr = htonl(addr);
	
	if (connect(_sock, (struct sockaddr *)&address, sizeof(address)) >= 0)
	{
		std::cout << "connect is established\n";
		_isOpened = true;
	}


}

// отправка сообщения 		
ssize_t Client::sendMsg(const void* msg, ssize_t size)
{
	if (_isOpened) 
	{ 
		std::cout << "sending message\n"; 
		return send(_sock, msg, size, 0);
	}
	return -1;
}

// прием сообщения
ssize_t Client::receiveMsg(void* msg, ssize_t size)
{
	if (_isOpened) return recv(_sock, msg, sizeof(msg), 0);
	return -1;
}

// закрытие соединения	
int Client::closeConnect() 
{
	if (_isOpened) {
		_isOpened = false; 
		std::cout << "connect is closed\n";
		return close(_sock);
	} 
	return -1; 
}
