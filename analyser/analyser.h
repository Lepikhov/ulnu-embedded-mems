#ifndef _ANALYSER_H
#define _ANALYSER_H


#include <stdint.h>
#include "protocol.h"




/// класс анализа данных от MEMS-модуля

class Analyser
{


public:
	/// коды ошибок	
	enum errors {
        	OK, ///< все хорошо
        	SYNC_ERROR, ///< неправильное синхрослово
        	CRC_ERROR,  ///< ошибка CRC
        	SIZE_ERROR, ///< ошибка размера
        	CYCLE_ERROR,///< ошибка счетчика циклов
        	UNDEFINED_ERROR ///< неизвестная ошибка
    	};

	/// конструктор класса, recNum - размер буфера записей данных от датчика MEMS 
	Analyser(size_t recNum);  
	~Analyser(); ///< деструктор класса


	/// вычисление CRC16
	uint16_t calcCRC16( const void * ptr , size_t size );

	/// функция ищет и анализирует сообщения от MEMS-модуля в буфере ar, с размером length  
	int check(const char * ar, size_t length); 

	/// очистка буфера с данными от датчика MEMS
	void clear() {_memsDataCnt = 0;}
	/// получение указателя на буфер с данными от датчика MEMS
	const mems_t * getMemsDataPtr() {return _memsDataPtr;} 
	/// получение текущего количества записей в буфере с данными от датчика MEMS
	size_t getMemsDataCnt() {return _memsDataCnt;} 

private:

	char _msgBuf[DEVICEMESSAGESIZE]; ///< буфер для головы сообщения 
	size_t _msgBufCnt; ///< счетчик байт в голове сообщения

	mems_t * _memsDataPtr; ///< указатель на буфер с данными от датчика MEMS
	size_t _memsDataSize;  ///< размер буфера с данными от датчика MEMS
	size_t _memsDataCnt;   ///< текущее количество записей в буфере с данными от датчика MEMS

	uint8_t _rCycle; ///< счетчик циклов сообщений от датчика MEMS	


	/// поиск образца sAr в массиве ar
	/// возвращает индекс начала образца sAr в ar или -1, если не найден	
	/// размер массива: arLength, образца: sArLength
	size_t _findSubArr(const char *ar, const char *sAr, size_t arLength, size_t sArLength); 

	/// функция проверки и разбора одного сообщения от датчика MEMS
	errors _parser(const char * msg);


	/// размер заголовка заголовка сообщения от датчика MEMS 
	size_t _sizeOfTitle() {return sizeof(MEMSTITLE)-1;}  // без символа окончания строки

	size_t _sizeOfMessage() {return DEVICEMESSAGESIZE;}  ///< размер сообщения от датчика MEMS 


};

#endif // _ANALYSER_H

