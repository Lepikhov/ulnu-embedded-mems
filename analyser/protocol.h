#ifndef _PROTOCOL_H
#define _PROTOCOL_H

/// заголовок сообщения от устройства
typedef struct
{
	uint8_t		sync[4];		///< ADOP - impulse sensor, MEMS - inertial
	uint16_t	cycle;			///< 0 - 0xFFFF
	uint16_t	size;			///< data size
	uint16_t	inv_size;		///< inv_size = ~ size
	uint32_t	res;			///< reserved
	uint16_t	crc16;			///< crc16
} header_t;

// заголовок заголовка (поле sync) сообщения от датчика MEMS
#define MEMSTITLE "MEMS"



/// тело сообщения от MEMS-модуля
typedef struct
{
	uint16_t	flags1;	///< error flags ADIS-Xmega communication 
						// (==0 all is ok)
	uint16_t	flags2; ///< error flags, read from ADIS
						// (==0 all is ok)
	uint16_t	x_gyro;	///< axis X gyro data				
	uint16_t	y_gyro;	///< axis Y gyro data
	uint16_t	z_gyro;	///< axis Z gyro data
	uint16_t	x_accl;	///< axis X accelerometer data
	uint16_t	y_accl;	///< axis Y accelerometer data
	uint16_t	z_accl;	///< axis Z accelerometer data
	uint16_t	temper;	///< ADIS temperature data
	uint16_t	cntr;	///< internal ADIS tick counter 
				// 1 tick ~ 40.69 us = 1/(24576 Hz)
} mems_t;

/// тело сообщения с длинными значениями показаний датчиков
typedef struct
{
	uint32_t	x_gyro;	///< axis X gyro data				
	uint32_t	y_gyro;	///< axis Y gyro data
	uint32_t	z_gyro;	///< axis Z gyro data
	uint32_t	x_accl;	///< axis X accelerometer data
	uint32_t	y_accl;	///< axis Y accelerometer data
	uint32_t	z_accl;	///< axis Z accelerometer data
	uint32_t	temper;	///< ADIS temperature data
} mems_long_t;

#define DEVICEMESSAGESIZE ((16)+(20)) // sizeof(header_t)+sizeof(mems_t) для платформы контроллера MEMS (XMega)
#define MEMSMESSAGESIZE (20) // sizeof(mems_t) для платформы контроллера MEMS (XMega)



#endif // _PROTOCOL_H
