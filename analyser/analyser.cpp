#include <iostream>
#include <string.h>
#include "analyser.h"

//#define _DEBUG

// конструктор класса, recNum - размер буфера записей данных от датчика MEMS 
Analyser::Analyser(size_t recNum) : _msgBufCnt(0), _memsDataSize(0), _memsDataCnt(0)
{
	
	_memsDataPtr = new mems_t[recNum]; // создаем буфер записей данных от датчика MEMS
	if (_memsDataPtr != NULL) _memsDataSize = recNum; // если удачно, то инициализируем размер буфера

} 

// деструктор класса
Analyser::~Analyser()
{
	if (_memsDataSize) delete _memsDataPtr; // удаляем буфер записей данных от датчика MEMS
}
 


// простой поиск образца sAr в массиве ar
// возвращает индекс начала образца sAr в ar или -1, если не найден	
// размер массива: arLength, образца: sArLength
size_t Analyser::_findSubArr(const char *ar, const char *sAr, size_t arLength, size_t sArLength)
{
	if (arLength < sArLength) return -1;

	// i-с какого места строки  ищем
	// j-с какого места образца ищем
	for (size_t i=0; i < arLength - sArLength + 1; ++i) {
		size_t j;
		for ( j=0; j < sArLength; ++j ) {
			if( ar[i+j]!=sAr[j] ) break;
		}
		if (j == sArLength) return i; // образец найден 
		// пока не найден, продолжим внешний цикл
	}
	// образца нет
	return -1;

}

// вычисление CRC16
uint16_t Analyser::calcCRC16( const void * ptr , size_t size )
{
	uint8_t *	ab, rabb;
	size_t		i,j;
	uint16_t		result;
	void *	av;
	char *	buffer = ( char * ) ptr;

	av			= & result;
	ab			= ( uint8_t * ) av;
	result	= 0xffff;

	for( i = 0 ; i < size ; i ++ )
	{
		rabb = ( uint8_t ) buffer[i];
		ab[0] = ab[0]^rabb;
		for( j = 0 ; j < 8 ; j++ )
		{
			if( result & 0x0001 ) { result >>= 1; result ^= 0xa001; }
			else result >>= 1;
		}
	}
	return result;	
}

// функция проверки и разбора одного сообщения от датчика MEMS
Analyser::errors Analyser::_parser(const char * msg)
{
	const char * ptr = msg;
	uint16_t tmp;

	if (memcmp(ptr, MEMSTITLE, _sizeOfTitle())) return Analyser::SYNC_ERROR; // неправильное синхрослово

	ptr += _sizeOfTitle();
	tmp = _rCycle;
	_rCycle = *((const uint16_t*)ptr);
	if (tmp == _rCycle) return Analyser::CYCLE_ERROR; // ошибка счетчика циклов 

	ptr += 2;
	tmp = *((const uint16_t*)ptr);
	ptr += 2;
	if (~(int16_t)tmp != (int16_t)*((const uint16_t*)ptr)) return Analyser::SIZE_ERROR; // ошибка размера
	if (tmp != MEMSMESSAGESIZE) return Analyser::SIZE_ERROR; // ошибка размера

	ptr += 2+4;
	tmp = *((const uint16_t*)ptr);
	ptr += 2;
	if (tmp != calcCRC16(ptr, MEMSMESSAGESIZE)) return Analyser::CRC_ERROR;  // ошибка CRC 


	mems_t m;
	
	m.flags1 = *((const uint16_t*)ptr);

	ptr += 2;
	m.flags2 = *((const uint16_t*)ptr);

	ptr += 2;
	m.x_gyro = *((const uint16_t*)ptr);

	ptr += 2;
	m.y_gyro = *((const uint16_t*)ptr);

	ptr += 2;
	m.z_gyro = *((const uint16_t*)ptr);

	ptr += 2;
	m.x_accl = *((const uint16_t*)ptr);

	ptr += 2;
	m.y_accl = *((const uint16_t*)ptr);

	ptr += 2;
	m.z_accl = *((const uint16_t*)ptr);

	ptr += 2;
	m.temper = *((const uint16_t*)ptr);

	ptr += 2;
	m.cntr = *((const uint16_t*)ptr);

#if 0
	std::cout.unsetf(std::ios::dec);
	std::cout.setf(std::ios::hex);	
	std::cout <<
	"\nf1=" << m.flags1  <<
	" f2=" << m.flags2  <<
	" xg=" << m.x_gyro  <<				
	" yg=" << m.y_gyro  <<
	" zg=" << m.z_gyro  <<
	" xa=" << m.x_accl  <<
	" ya=" << m.y_accl  <<
	" za=" << m.z_accl  <<
	" t=" << m.temper  <<
	" cnt=" << m.cntr << "\n";
	std::cout.unsetf(std::ios::hex);
#endif
	
	if (_memsDataCnt < _memsDataSize) memcpy(&_memsDataPtr[_memsDataCnt++], &m, sizeof(m));		
	//std::cout << "parser cnt=" << _memsDataCnt << "\n";

	return Analyser::OK;
}

// функция ищет и анализирует сообщения от MEMS-модуля в буфере ar, с размером length  
int Analyser::check(const char * ar, size_t length)
{
	size_t p = 0;
	const char * ptr = ar;
	size_t j;
	//Analyser::errors er;

	//std::cout <<"an->check calling...\n";

#ifdef _DEBUG	
	int cnt = 0;
			std::cout << "\ncheck input buf:\n";
			std::cout.unsetf(std::ios::dec);
			std::cout.setf(std::ios::hex);	
			for (size_t k=0; k < length; ++k) 
			{
				std::cout << (0xff & (uint16_t)ptr[k]) << " ";			
			}
			std::cout.unsetf(std::ios::hex);
#endif

	if (_msgBufCnt) // если в буфере есть голова предыдущего сообщения
	{
		if (_msgBufCnt < _sizeOfMessage())
		{
			p = _sizeOfMessage()-_msgBufCnt;
//			std::cout << "tail=" << p << "\n";
			for (j=0; j < p; ++j) 
			{
				_msgBuf[j+_msgBufCnt] = ptr[j];
				if (j >= length) break;
			}
			length -= j;

#ifdef _DEBUG
			std::cout << "\nfrom msgBuf:\n";
			std::cout.unsetf(std::ios::dec);
			std::cout.setf(std::ios::hex);
			for (j=0; j < _sizeOfMessage(); ++j) 
			{
				std::cout << (0xff & (uint16_t)_msgBuf[j]) << " ";
			}
			std::cout.unsetf(std::ios::hex);
#endif	
			//er = 
			_parser(_msgBuf);		
			//if (er != Analyser::OK) std::cout <<"pars err = " << er << "\n";	
		}
		_msgBufCnt = 0;
		ptr += p; 		
	}

		
	while (p < length)
	{

		if (length-p < _sizeOfTitle()) // в буфере остался кусок с длиной меньше заголовка
		{
			j = length - p;
			memcpy(_msgBuf,ptr,j);
			_msgBufCnt = j;
//			std::cout << "msgBufCnt=" << _msgBufCnt << "\n";
#ifdef _DEBUG	
			std::cout.unsetf(std::ios::dec);
			std::cout.setf(std::ios::hex);	
			for (size_t k=0; k < j; ++k) 
			{
				std::cout << (0xff & (uint16_t)ptr[k]) << " ";			
			}
			std::cout.unsetf(std::ios::hex);
#endif	
			break;
		}

		p = _findSubArr(ptr, MEMSTITLE, length-p, _sizeOfTitle());
//		std::cout << "\np=" << p << " length=" << length << "\n";
		if (p == (size_t)-1) return 0;

		ptr += p;

		if (p+_sizeOfMessage() > length) // неполное сообщение в буфере
		{
			j = length - p;
			memcpy(_msgBuf,ptr,j);
			_msgBufCnt = j;
//			std::cout << "msgBufCnt=" << _msgBufCnt << "\n";
#ifdef _DEBUG	
			std::cout.unsetf(std::ios::dec);
			std::cout.setf(std::ios::hex);	
			for (size_t k=0; k < j; ++k) 
			{
				std::cout << (0xff & (uint16_t)ptr[k]) << " ";			
			}
			std::cout.unsetf(std::ios::hex);
#endif	
		}
		else
		{
#ifdef _DEBUG	
			std::cout << "\ncnt=" << ++cnt << ":\n";
			std::cout.unsetf(std::ios::dec);
			std::cout.setf(std::ios::hex);	
			for (j=0; j < _sizeOfMessage(); ++j) 
			{
				std::cout << (0xff & (uint16_t)ptr[j]) << " ";			
			}
			std::cout.unsetf(std::ios::hex);
#endif			
			//er = 
			_parser(ptr);		
			//if (er != Analyser::OK) std::cout <<"pars err = " << er << "\n";		
			j = _sizeOfMessage();


		}

		ptr += j; 
		length -= j+p;

	}

	return 0;
} 

