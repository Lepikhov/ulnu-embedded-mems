#include <fcntl.h>
#include <termios.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>  
#include <unistd.h>
#include <sys/ioctl.h>

#include <iostream> 

#include "serialfunc.h"


/*----------------------------------------------------------------------------
 Открыть COM порт
 COM_name: путь к устройству, например: /dev/ttyS0 или  /dev/ttyUSB0 - для USB
 speed: скорость, например: 9600, 57600, 115200 
 ----------------------------------------------------------------------------*/
bool openPort(int *f_id, const char *COM_name, unsigned long speed)
{
    speed_t BAUD;	
    *f_id = open(COM_name, O_RDWR | O_NOCTTY);
    if(*f_id == -1)
    {
			char *errmsg = strerror(errno);
			std::cerr << errmsg << "\n";
			return false;
    }
    struct termios options; /*структура для установки порта*/
    tcgetattr(*f_id, &options); /*читает параметры порта*/


     switch (speed)
     {
         case 921600:
            BAUD  = B921600;
            break;
         case 460800:
            BAUD  = B460800;
            break;
         case 230400:
            BAUD  = B230400;
            break;
         case 115200:
            BAUD  = B115200;
            break;
         case 57600:
            BAUD  = B57600;
            break;
         case 38400:
         default:
            BAUD = B38400;
            break;
         case 19200:
            BAUD  = B19200;
            break;
         case 9600:
            BAUD  = B9600;
            break;
         case 4800:
            BAUD  = B4800;
            break;
         case 2400:
            BAUD  = B2400;
            break;
         case 1800:
            BAUD  = B1800;
            break;
         case 1200:
            BAUD  = B1200;
            break;
         case 600:
            BAUD  = B600;
            break;
         case 300:
            BAUD  = B300;
            break;
         case 200:
            BAUD  = B200;
            break;
         case 150:
            BAUD  = B150;
            break;
         case 134:
            BAUD  = B134;
            break;
         case 110:
            BAUD  = B110;
            break;
         case 75:
            BAUD  = B75;
            break;
         case 50:
            BAUD  = B50;
            break;
      }  //end of switch baud_rate

    cfsetispeed(&options, BAUD); /*установка скорости порта*/
    cfsetospeed(&options, BAUD); /*установка скорости порта*/

    options.c_cc[VTIME]    = VTIMEPERIOD; /*Время ожидания байта  */
    options.c_cc[VMIN]     = 0; /*минимальное число байт для чтения*/

 
    options.c_cflag &= ~PARENB; /*бит четности не используется*/
    options.c_cflag &= ~CSTOPB; /*1 стоп бит */
    options.c_cflag &= ~CSIZE;  /*Размер байта*/
    options.c_cflag |= CS8;  /*8 бит*/
    
    options.c_lflag = 0;
    options.c_oflag &= ~OPOST; /*Обязательно отключить постобработку*/

    tcsetattr(*f_id, TCSANOW, &options); /*сохранения параметров порта*/
    
    return true;
}

/*----------------------------------------------------------------------------
 Прочитать данные из COM порта
 buff - буфер для принятых данных
 size - количество запрашиваемых байт
 ----------------------------------------------------------------------------*/
int readData(int *f_id, unsigned char *buff,int size)
{
	bool ok = false;
	int n;	
	
	while (!ok)
	{
		n = read(*f_id, buff, size);
	  //std::cout << "n=" << n << "\n";
		if (n == -1)
		{
			int er = errno;

	    char *errmsg = strerror(er);
			std::cerr << errmsg << "\n";

			if (er == EINTR) continue; // функция прервана сигналом (например, от таймера) 


		}
		ok = true;
	}

	return n;
}


int clrInputBuffer(int *f_id)
{
	return tcflush(*f_id, TCIFLUSH);
}

/*----------------------------------------------------------------------------
 Отправить в COM порт данные
 buff - буфер данных для отправки
 size - количество отправляемых байт
 ----------------------------------------------------------------------------*/
int sendData(int *f_id, unsigned char* buff,int len)
{
    int n = write(*f_id, buff, len);
    if(n == -1)
    {
    	char *errmsg = strerror(errno);
			std::cerr << errmsg << "\n";
    }
    return n;
}
/*----------------------------------------------------------------------------
 Закрыть COM порт
 ----------------------------------------------------------------------------*/
void closeCom(int *f_id)
{
    close(*f_id);
    *f_id = -1; 
    return;
}
/*----------------------------------------------------------------------------
 Установить RTS
 ----------------------------------------------------------------------------*/
void setRTS(int *f_id)
{
    int status;
    ioctl(*f_id, TIOCMGET, &status);
    status |= TIOCM_RTS;
    ioctl(*f_id, TIOCMSET, &status);
}
/*----------------------------------------------------------------------------
Очистить RTS
 ----------------------------------------------------------------------------*/
void clrRTS(int *f_id)
{
    int status;
    ioctl(*f_id, TIOCMGET, &status);
    status &= ~TIOCM_RTS;
    ioctl(*f_id, TIOCMSET, &status);
}

